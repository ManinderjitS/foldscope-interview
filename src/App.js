// import TextBox from './components/text_box/TextBox';
import {useState} from 'react';

import CharList from './components/char_list/CharList';

import './index.css';

function App() {
    
    const [text, setText] = useState('');
    const [analyzing, setAnalyzing] = useState(false);
    const [listReady, setListReady] = useState(false);
    const [charArrFinal, setCharArrFinal] =  useState([]);
    const [charMapFinal, setCharMapFinal] = useState({});
    
    let top5Chars = [];   // [  [char_count, 'ch'], ..... ]
    let charArr = [];
    let charMap = {};
    let top5CharCount = 0;
    
    const onChangeText = (e) => {
        setText(e.target.value);
    }
    
    const onSubmitHandler = (e) => {
        e.preventDefault();
        setAnalyzing(true);
        analyzeText();
    }
    
    const analyzeText = () => {
        let ch = '';
        
        for(let i = 0; i < text.length; i++){        
            ch = text.charAt(i);
            
            if(ch === ' '){
                ch = 'space';
            }
            
            //Check if the char is already in the 
            if(charMap[ch] !== undefined){
                charMap[ch].char_count++;
                updateTop5(ch, charMap[ch].char_count);
            }
            else {
                charMap[ch] = {char_count : 1};
                charArr.push(ch);
                insertTop5(ch, 1);
            }
        }
        
        setCharMapFinal(charMap);
        setCharArrFinal(charArr);
        
        charMap = {};
        charArr = [];
        top5Chars = [];
        top5CharCount = 0;
        setText('');
        
        setAnalyzing(false);
        setListReady(true);
    }
    
    const insertTop5 = (ch, count) => {
        let top5Ind = 0;
        if(top5CharCount < 5){
            top5Ind = top5Chars.push([count, ch]) - 1;
            
            //Save the top 5 position 
            charMap[ch]['top_5_ind'] = top5Ind;
            charMap[ch]['in_top_5'] = true;
            top5CharCount++;
        }
    }
    
    const updateTop5 = (ch, count) => {
        if(charMap[ch]['top_5_ind'] !== undefined){//Char already in top5
            
            let ind = charMap[ch]['top_5_ind'];
            top5Chars[ind][0] = count;
            
            //Now we resort (ascending order)
            for(let i = ind; i < 4; i++){
                if(top5Chars[i + 1] !== undefined && top5Chars[i][0] > top5Chars[i + 1][0]){
                    //Move up in list
                    let tmp = top5Chars[i + 1];
                    top5Chars[i + 1] = top5Chars[i];
                    top5Chars[i] = tmp;
                    
                    //update the index of both
                    charMap[top5Chars[i][1]]['top_5_ind'] = i;
                    charMap[top5Chars[i + 1][1]]['top_5_ind'] = i + 1;
                }
                else{
                    break;
                }
            }
        }
        else{
            //Check if the new char count is bigger than the lowest char count in top5
            if(count > top5Chars[0][0]){
                let insert_at = 1;
                //Now we add the item in a sorted fashion (ascending order)
                for(let i = 1; i < 5; i++){
                    if(count > top5Chars[i][0]){
                        insert_at++;
                        charMap[top5Chars[i][1]]['top_5_ind'] = i - 1; 
                    }
                }
                
                //Remove the lowest count char
                delete charMap[top5Chars[0][1]]['top_5_ind'];
                delete charMap[top5Chars[0][1]]['in_top_5'];
                top5Chars.shift();
                
                top5Chars.splice(insert_at - 1, 0, [count, ch]);
                charMap[ch]['top_5_ind'] = insert_at - 1;
                charMap[ch]['in_top_5'] = true;
            }
        }
    }
    
    return (
        <div className="App h-screen w-full flex flex-col justify-center items-center font-mono">
            <>
                <h1 className="font-bold text-center text-gray-900">Please Input Text Below</h1>
                
                <section className="mx-auto my-10 bg-white border-2 border-gray-300 p-6 rounded-md tracking-wide shadow-lg">
                    <div 
                        id="header" 
                        className="mb-4"> 
                        <span>
                            <form
                                onSubmit={onSubmitHandler}>
                                <input 
                                    className="h-[7rem] mx-4 p-2 outline-none border border-gray-300 rounded-md focus:border-gray-400 px-10 py-4 overflow-hidden overflow-y-auto"
                                    type="text"
                                    id="textBox"
                                    name="textBox"
                                    value={text}
                                    onChange={(e) => onChangeText(e)}/>
                                    
                                <button 
                                    className="text-xs bg-blue-500 rounded-lg font-bold text-white text-center px-4 py-3 mt-2 mx-2 item-center transition duration-300 ease-in-out hover:bg-blue-600 mr-6"
                                    type="submit">
                                    Analyze Text
                                </button>
                            </form>
                        </span>
                    </div>
                </section>
                
                <div>
                    {analyzing && <h2>Analyzing...</h2>}            
                    {listReady && <CharList charMap={charMapFinal} charArr={charArrFinal}/>}
                </div>
            </>
        </div>
    );
}

export default App;
