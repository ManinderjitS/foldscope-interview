import PropTypes from 'prop-types';

const CharList = ({charArr, charMap}) => {
    
    return (
        <>
            <div>
                <h1> <span className="bg-blue-400 text-white px-2 rounded-md">Blue</span> boxes are showing the top 5 chars.</h1>
                <div className="bg-white rounded-lg w-1/6 container mb-2 flex mx-auto w-full items-center justify-center">
                    <ul className="divide-y text-center overscroll-auto h-32">
                        {   
                            charArr.map((char) => {
                                return (
                                    <li 
                                        className={charMap[char].in_top_5 ? "p-2 rounded-lg text-center bg-blue-500 text-white mt-1" : "mt-1 bg-gray-200 p-2 text-center rounded-lg"}
                                        key={char}>
                                        '{char}' - {charMap[char].char_count}                         
                                    </li>
                                );
                            })
                        }
                    </ul>
                </div>
            </div>
        </>
    );
}

CharList.propTypes = {
    charArr: PropTypes.array.isRequired,
    charMap: PropTypes.object.isRequired
};

export default CharList;
